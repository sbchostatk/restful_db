package com.java.db.restful_db.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "Teacher_Student")
public class TeacherStudent {

    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "teacherStudentIdSeq", sequenceName = "teacher_student_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "teacherStudentIdSeq")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "teacher_id", nullable = false)
    private Teacher teacher;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id", nullable = false)
    private Student student;


    public TeacherStudent() {

    }

    public TeacherStudent(Teacher teacher, Student student) {
        this.teacher = teacher;
        this.student = student;
    }

    public int getId() {
        return id;
    }

    @JsonIgnore
    public Teacher getTeacher() {
        return teacher;
    }

    @JsonIgnore
    public Student getStudent() {
        return student;
    }
}
