package com.java.db.restful_db.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
@Table(name = "Teacher")
public class Teacher {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "teacher_id")
    @SequenceGenerator(name = "teacherIdSeq", sequenceName = "teacher_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "teacherIdSeq")
    private int teacher_id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String middle_name;

    @Column(nullable = false)
    private String last_name;

    @Column
    private String subject;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "teacher")
    private List<TeacherStudent> teacherStudentList;

    protected Teacher() {
    }

    public Teacher(String name, String middle_name, String last_name, String subject) {
        super();
        this.name = name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.subject = subject;
        teacherStudentList = new ArrayList<>();
    }

    public String getName() {
        return this.name;
    }

    public int getId() {
        return teacher_id;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getSubject() {
        return subject;
    }

    @Override
    public String toString() {
        return getName() + " " + getMiddle_name() + " " + getLast_name() +
                ", предмет: " + getSubject();
    }
}
