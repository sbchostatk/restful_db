package com.java.db.restful_db.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
@Table(name = "Student")
public class Student {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "student_id")
    @SequenceGenerator(name = "studentIdSeq", sequenceName = "student_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "studentIdSeq")
    private int student_id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String middle_name;

    @Column(nullable = false)
    private String last_name;

    @Column(nullable = false)
    private int course;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "student")
    private List<TeacherStudent> teacherStudentList;

    protected Student() {}

    public Student(String name, String middle_name, String last_name, int course) {
        super();
        this.name = name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.course = course;
        teacherStudentList = new ArrayList<TeacherStudent>();
    }

    public int getId() {
        return student_id;
    }

    public String getName() {
        return name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public int getCourse() {
        return course;
    }

    @Override
    public String toString() {
        return getName() + " " + getMiddle_name() + " " + getLast_name() +
                ", " + getCourse() + " курс";
    }
}
