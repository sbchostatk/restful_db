package com.java.db.restful_db.service;

import com.java.db.restful_db.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentsRepository extends JpaRepository<Student, Integer> {
}
