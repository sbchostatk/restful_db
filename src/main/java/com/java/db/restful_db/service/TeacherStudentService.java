package com.java.db.restful_db.service;

import com.java.db.restful_db.domain.Student;
import com.java.db.restful_db.domain.Teacher;
import com.java.db.restful_db.domain.TeacherStudent;

import java.util.List;

public interface TeacherStudentService {

    void create(TeacherStudent teacherStudent);

    List<Student> getStudents(Teacher teacher);

    List<Teacher> getTeachers(Student student);

    boolean delete(Teacher teacher, Student student);
}
