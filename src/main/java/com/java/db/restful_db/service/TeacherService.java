package com.java.db.restful_db.service;

import com.java.db.restful_db.domain.Student;
import com.java.db.restful_db.domain.Teacher;
import org.springframework.data.domain.Page;

import java.util.List;

public interface TeacherService {
    Teacher getTeacher(int id);

    void create(Teacher teacher);

    List<Teacher> getAll();

    boolean delete(int id);

}
