package com.java.db.restful_db.service;

import com.java.db.restful_db.domain.Student;
import com.java.db.restful_db.domain.Teacher;
import com.java.db.restful_db.domain.TeacherStudent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TeacherStudentRepository extends JpaRepository<TeacherStudent, Integer> {
    TeacherStudent findByTeacherAndStudent(Teacher teacher, Student student);
    List<TeacherStudent> findAllByTeacher(Teacher teacher);

    List<TeacherStudent> findAllByStudent(Student student);
}
