package com.java.db.restful_db.service;

import com.java.db.restful_db.domain.Student;

import java.util.List;

public interface StudentService {

    Student getStudent(int id);

    void create(Student student);

    List<Student> getAll();

    boolean delete(int id);
}
