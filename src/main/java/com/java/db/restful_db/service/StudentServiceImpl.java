package com.java.db.restful_db.service;

import com.java.db.restful_db.domain.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentsRepository studentsRepository;

    @Autowired
    public StudentServiceImpl(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    //получить студента
    @Override
    public Student getStudent(int id) {
        return studentsRepository.findById(id).get();
    }

    //добавить студента
    @Override
    public void create(Student student) {
        studentsRepository.save(student);
    }

    //удалить студента
    @Override
    public boolean delete(int id) {
        if (studentsRepository.existsById(id)) {
            studentsRepository.deleteById(id);
            return true;
        }
        return false;
    }

    //получить всех студентов
    @Override
    public List<Student> getAll() {
        return studentsRepository.findAll();
    }
}
