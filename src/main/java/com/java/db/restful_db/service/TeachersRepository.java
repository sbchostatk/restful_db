package com.java.db.restful_db.service;

import com.java.db.restful_db.domain.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TeachersRepository extends JpaRepository<Teacher, Integer> {

}
