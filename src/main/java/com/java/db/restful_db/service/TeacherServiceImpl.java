package com.java.db.restful_db.service;

import com.java.db.restful_db.domain.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService {

    private final TeachersRepository teachersRepository;

    @Autowired
    public TeacherServiceImpl(TeachersRepository teachersRepository){
        this.teachersRepository = teachersRepository;
    }


    @Override
    public Teacher getTeacher(int id) {
        return teachersRepository.findById(id).get();
    }

    @Override
    public void create(Teacher teacher) {
        teachersRepository.save(teacher);
    }

    @Override
    public List<Teacher> getAll() {
        return teachersRepository.findAll();
    }

    @Override
    public boolean delete(int id) {
        if (teachersRepository.existsById(id)) {
            teachersRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
