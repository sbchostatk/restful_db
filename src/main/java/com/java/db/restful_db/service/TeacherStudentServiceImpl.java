package com.java.db.restful_db.service;

import com.java.db.restful_db.domain.Student;
import com.java.db.restful_db.domain.Teacher;
import com.java.db.restful_db.domain.TeacherStudent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TeacherStudentServiceImpl implements TeacherStudentService {
    @Autowired
    private final TeacherStudentRepository teacherStudentRepository;

    @Autowired
    private final StudentsRepository studentRepository;

    @Autowired
    private final TeachersRepository teacherRepository;

    public TeacherStudentServiceImpl(TeacherStudentRepository teacherStudentRepository, StudentsRepository studentRepository, TeachersRepository teacherRepository) {
        this.teacherStudentRepository = teacherStudentRepository;
        this.studentRepository = studentRepository;
        this.teacherRepository = teacherRepository;
    }

    @Override
    public void create(TeacherStudent teacherStudent) {
        teacherStudentRepository.save(teacherStudent);
    }

    @Override
    public List<Student> getStudents(Teacher teacher) {
        List<TeacherStudent> teacherStudentList = teacherStudentRepository.findAllByTeacher(teacher);
        List<Student> students = new ArrayList<>();
        for (TeacherStudent teacherStudent : teacherStudentList) {
            students.add(teacherStudent.getStudent());
        }
        return students;
    }

    @Override
    public List<Teacher> getTeachers(Student student) {
        List<TeacherStudent> teacherStudentList = teacherStudentRepository.findAllByStudent(student);
        List<Teacher> teachers = new ArrayList<>();
        for (TeacherStudent teacherStudent : teacherStudentList) {
            teachers.add(teacherStudent.getTeacher());
        }
        return teachers;
    }

    @Override
    public boolean delete(Teacher teacher, Student student) {
        TeacherStudent teacherStudent = teacherStudentRepository.findByTeacherAndStudent(teacher, student);
        if (teacherStudent != null) {
            teacherStudentRepository.delete(teacherStudent);
            return true;
        }
        return false;
    }
}
