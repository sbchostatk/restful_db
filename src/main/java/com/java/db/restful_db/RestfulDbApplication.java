package com.java.db.restful_db;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfulDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestfulDbApplication.class, args);
    }

}
