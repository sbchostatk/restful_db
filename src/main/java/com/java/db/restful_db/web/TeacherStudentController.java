package com.java.db.restful_db.web;

import com.java.db.restful_db.domain.Student;
import com.java.db.restful_db.domain.Teacher;
import com.java.db.restful_db.domain.TeacherStudent;
import com.java.db.restful_db.service.StudentService;
import com.java.db.restful_db.service.TeacherService;
import com.java.db.restful_db.service.TeacherStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/connection")
public class TeacherStudentController {
    @Autowired
    private TeacherStudentService teacherStudentService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private TeacherService teacherService;

    @PostMapping(value = "/add")
    public ResponseEntity<?> createConnection(@RequestParam(name = "teacherId", required = false) int teacher_id,
                                              @RequestParam(name = "studentId", required = false) int student_id) {

        if (teacherService.getTeacher(teacher_id) == null || studentService.getStudent(student_id) == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else {
            TeacherStudent teacherStudent = new TeacherStudent(teacherService.getTeacher(teacher_id),
                    studentService.getStudent(student_id));
            teacherStudentService.create(teacherStudent);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
    }

    @PostMapping(value = "/delete")
    public ResponseEntity<?> deleteConnection(@RequestParam(name = "teacherId", required = false) int teacher_id,
                                              @RequestParam(name = "studentId", required = false) int student_id) {

        final boolean deleted = teacherStudentService.delete(teacherService.getTeacher(teacher_id),
                studentService.getStudent(student_id));

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @GetMapping(value = "/getStudents/{id}")
    public ResponseEntity<List<Student>> getStudents(@PathVariable(name = "id") int teacher_id) {
        final List<Student> students = teacherStudentService.getStudents(teacherService.getTeacher(teacher_id));

        return students != null && !students.isEmpty()
                ? new ResponseEntity<>(students, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/getTeachers/{id}")
    public ResponseEntity<List<Teacher>> getTeachers(@PathVariable(name = "id") int student_id) {
        final List<Teacher> teachers = teacherStudentService.getTeachers(studentService.getStudent(student_id));

        return teachers != null && !teachers.isEmpty()
                ? new ResponseEntity<>(teachers, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /*@RequestMapping("/students")
    @ResponseBody
    @Transactional(readOnly = true)
    public void getAllStudents() {
        System.out.println(this.studentServiceImpl.getAll());
    }*/
}
