/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.java.db.restful_db.web;

import com.java.db.restful_db.domain.Student;
import com.java.db.restful_db.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/students")
public class StudentController {

	@Autowired
	private StudentService studentService;

	//добавить студента
	@PostMapping(value = "/add")
	public ResponseEntity<?> addStudent(@RequestBody Student student) {

		studentService.create(student);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	//удалить студента
	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity<?> deleteStudent(@PathVariable(name = "id") int id) {
		final boolean deleted = studentService.delete(id);

		return deleted
				? new ResponseEntity<>(HttpStatus.OK)
				: new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
	}

	//получить одного студента
	@GetMapping(value = "/getOne/{id}")
	public ResponseEntity<Student> getStudent(@PathVariable(name = "id") int id) {
		final Student student = studentService.getStudent(id);

		return student != null
				? new ResponseEntity<>(student, HttpStatus.OK)
				: new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	//получить всех студентов
	@RequestMapping("/getAll")
	public ResponseEntity<List<Student>> getAllStudents() {
		final List<Student> students = studentService.getAll();

		return students != null && !students.isEmpty()
				? new ResponseEntity<>(students, HttpStatus.OK)
				: new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}
