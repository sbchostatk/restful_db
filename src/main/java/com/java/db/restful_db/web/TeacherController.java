package com.java.db.restful_db.web;

import com.java.db.restful_db.domain.Student;
import com.java.db.restful_db.domain.Teacher;
import com.java.db.restful_db.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/teachers")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    //добавить студента
    @PostMapping(value = "/add")
    public ResponseEntity<?> addTeacher(@RequestBody Teacher teacher) {
        teacherService.create(teacher);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    //удалить студента
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<?> deleteTeacher(@PathVariable(name = "id") int id) {
        final boolean deleted = teacherService.delete(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    //получить одного студента
    @GetMapping(value = "/getOne/{id}")
    public ResponseEntity<Teacher> getTeacher(@PathVariable(name = "id") int id) {
        final Teacher teacher = teacherService.getTeacher(id);

        return teacher != null
                ? new ResponseEntity<>(teacher, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //получить всех студентов
    @RequestMapping("/getAll")
    public ResponseEntity<List<Teacher>> getAllTeachers() {
        final List<Teacher> teachers = teacherService.getAll();

        return teachers != null && !teachers.isEmpty()
                ? new ResponseEntity<>(teachers, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


}
