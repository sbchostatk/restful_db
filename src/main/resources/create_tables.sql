create table Teacher (
                         id int primary key not null unique,
                         name varchar(30) not null,
                         middle_name varchar(30) not null,
                         last_name varchar(30) not null,
                         subject varchar(30)
);

alter table teacher rename column id to teacher_id;

alter table student rename column id to student_id;

create table Student (
                         id int primary key not null unique,
                         name varchar(30) not null,
                         middle_name varchar(30) not null,
                         last_name varchar(30) not null,
                         course int not null
);

create table Teacher_Student (
                                 id int primary key not null unique,
                                 teacher_id int references Teacher(id) not null,
                                 student_id int references Student(id) not null
);

CREATE SEQUENCE student_id_seq START WITH 2 INCREMENT BY 1;
CREATE SEQUENCE teacher_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE teacher_student_id_seq START WITH 1 INCREMENT BY 1;